;; Add Melpa packages
(require 'package)
(add-to-list
 'package-archives
 '("melpa" . "http://melpa.org/packages/")
 t)
(package-initialize)


;; use-package helps with auto-installing packages
(if (not (package-installed-p 'use-package))
    (progn
      (package-refresh-contents)
      (package-install 'use-package)))

(require 'use-package)


(load "~/emacs.d/theme.el")
(load "~/emacs.d/general.el")
(load "~/emacs.d/terminal.el")
(load "~/emacs.d/whitespace.el")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;           Evil             ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load "~/emacs.d/evil.el")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;         Post Evil          ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;



;;; Below here probably requires evil.
(load "~/emacs.d/commands.el")
(load "~/emacs.d/treemacs.el")
(load "~/emacs.d/ivy.el")
(load "~/emacs.d/git.el")
(load "~/emacs.d/ediff.el")


;; langs
(load "~/emacs.d/c.el")
(load "~/emacs.d/markdown.el")
(load "~/emacs.d/haskell.el")
(load "~/emacs.d/golang.el")
(load "~/emacs.d/tex.el")
(load "~/emacs.d/python.el")
(load "~/emacs.d/org.el")
(load "~/emacs.d/kubernetes.el")


(use-package magit :ensure t)

(setq git-commit-known-pseudo-headers
  '("Signed-off-by"
    "Acked-by"
    "Modified-by"
    "Cc"
    "Suggested-by"
    "Reported-by"
    "Tested-by"
    "Reviewed-by"
    "Co-authored-by"
    "chore"
    "docs"
    "feat"
    "fix"
    "refactor"
    "style"
    "test"))


(use-package cl :ensure t)
(defun git-commit-check-style-conventions (force)
  "Check for violations of certain basic style conventions.
For each violation ask the user if she wants to proceed anyway.
Option `git-commit-check-style-conventions' controls which
conventions are checked."
  (or force
      (save-excursion
        (goto-char (point-min))
        (re-search-forward (git-commit-summary-regexp) nil t)
        (if (equal (match-string 1) "")
            t ; Just try; we don't know whether --allow-empty-message was used.
          (and
               (or 
                (some (lambda (x) (string-match-p (concat "^" x "(?.*)?:") (match-string 1)))
                      '("chore"
                        "docs"
                        "feat"
                        "fix"
                        "refactor"
                        "style"
                        "test"))
                   (y-or-n-p "No semantic message. (eg. 'fix:')  Commit anyway? "))
               (or (not (memq 'overlong-summary-line
                              git-commit-style-convention-checks))
                   (equal (match-string 2) "")
                   (y-or-n-p "Summary line is too long.  Commit anyway? "))
               (or (not (memq 'non-empty-second-line
                              git-commit-style-convention-checks))
                   (not (match-string 3))
                   (y-or-n-p "Second line is not empty.  Commit anyway? ")))))))


(use-package git-gutter-fringe :ensure t)
(require 'git-gutter-fringe)
(global-git-gutter-mode t)

(set-face-attribute 'fringe nil :background "#3a3c48")

(use-package gitlab-ci-mode    :ensure t)



;; chore, docs, feat, fix, refactor, style, or test.
;; https://seesparkbox.com/foundry/semantic_commit_messages

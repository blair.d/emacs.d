
(when (window-system)
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (tooltip-mode -1)
  (show-paren-mode 1))

(setq-default tab-width 4)
(column-number-mode)



(display-battery-mode)
(display-time)
(setq display-time-format "%a %d, %H:%M")
(setq display-time-default-load-average nil)
(setq global-mode-string (remove 'display-time-string global-mode-string))
(setq mode-line-end-spaces (list (propertize " " 'display '(space :align-to (- right 14))) 'display-time-string))






(define-fringe-bitmap 'right-curly-arrow
  [#b00000000
   #b00000000
   #b00000000
   #b00000000
   #b01110000
   #b00010000
   #b00010000
   #b00000000])
(define-fringe-bitmap 'left-curly-arrow
  [#b00000000
   #b00000000
   #b00000000
   #b00000000
   #b10000000
   #b10000000
   #b11100000
   #b00000000])


(defun xterm-title-update ()
  (interactive)
  (unless (display-graphic-p)
    (send-string-to-terminal (concat "\033]1; " (buffer-name) "\007"))
    (if buffer-file-name
        (send-string-to-terminal (concat "\033]2; " (buffer-file-name) "\007"))
      (send-string-to-terminal (concat "\033]2; " (buffer-name) "\007")))))

(if (not window-system)
        (add-hook 'post-command-hook 'xterm-title-update))









;;; Fully undo the effects of one theme when loading
(defadvice load-theme
    (before theme-dont-propagate activate)
  (mapc #'disable-theme custom-enabled-themes))
(defadvice load-theme
    (after theme-dont-propagate activate))

;; The Best Theme
;;(load-theme 'soothe)

;(load-theme 'leuven)
(load "/home/blair/emacs.d/themes/emacs-leuven-theme/leuven-dark-theme.el")
;;(load-theme 'leuven-dark)
;;(load "~/emacs.d/themes/emacs-leuven-theme/kit-dark-theme.el")

(setq leuven-scale-outline-headlines nil)
(setq leuven-scale-org-agenda-structure nil)





(if (daemonp)
    (add-hook
     'after-make-frame-functions
     (lambda (frame)
       (select-frame frame)
       (ignore-errors
         (when (display-graphic-p)
           (set-frame-font "Terminus-12:bold")
           (menu-bar-mode -1)
           (tool-bar-mode -1)
           (scroll-bar-mode -1)
           (tooltip-mode -1)
           (show-paren-mode 1)
           (load-theme 'doom-one))
         (if (and (eq window-system nil) (not (equal server-name "gui")))
             (progn
               (set-face-attribute 'mode-line nil :background "brightblack" :foreground "white")
               (set-face-attribute 'mode-line-inactive nil :background "black" :foreground "brightwhite")
               (xterm-mouse-mode t)))))))



;; Rainbow Brackets
(use-package rainbow-delimiters
  :ensure t
  :config
  (mapc (lambda (x) (add-hook x #'rainbow-delimiters-mode))
        '(
          python-mode-hook
          latex-mode-hook
          TeX-mode-hook
          markdown-mode-hook
          emacs-lisp-mode-hook
          haskell-mode-hook
          ess-mode-hook
          )))

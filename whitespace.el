

(require 'whitespace)
;;; (setq whitespace-style '(face tabs trailing tab-mark))
;(setq whitespace-style '(face tabs))
;(set-face-attribute 'whitespace-tab nil
;			:background "grey"
;			:foreground "brightblack"
;			:weight 'bold)
;(set-face-attribute 'whitespace-trailing nil
;					:background "color-254"
;			:foreground "brightwhite"
;			:weight 'bold)
;(set-face-attribute 'whitespace-space-before-tab nil
;			:background "color-254"
;			:foreground "white"
;			:weight 'bold)
;(set-face-attribute 'whitespace-space-after-tab nil
;			:background "color-254"
;			:foreground "brightwhite"
;			:weight 'bold)

(add-hook 'font-lock-mode-hook  
		  (lambda ()  
			(font-lock-add-keywords  
			 nil  
			 '(("\t" 0 'trailing-whitespace prepend)))))

;(add-hook 'prog-mode-hook 'whitespace-mode)

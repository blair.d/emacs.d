
(defun my-pipe-command (beg end)
  "Send region into pipe"
  (interactive "*r")
  (let ((cmd (read-string "command:")))
    (if (region-active-p)
	(let ((result (shell-command-on-region beg end cmd)))
	  (delete-region (region-beginning) (region-end))
	  (insert result))
      (let ((result (shell-command-on-region (point-min) (point-max) cmd)))
	  (delete-region (point-min) (point-max))
	  (insert result)))))


;; evil's ":!" command
(defun my-pipe-command ()
  "Asks for a command and executes it in inferior shell with current buffer
as input."
  (interactive)
  (let ((current-prefix-arg '(4)))
    (call-interactively #'shell-command-on-region)))

(global-set-key "\M-\\" 'my-pipe-command)

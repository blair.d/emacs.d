(use-package elpy :ensure t)
(elpy-enable)

(when (executable-find "ipython")
  (setq python-shell-interpreter "ipython"))

;; use flycheck not flymake with elpy
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; enable autopep8 formatting on save
(use-package py-autopep8 :ensure t)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

(setq python-shell-interpreter "ipython3"
      python-shell-interpreter-args "--simple-prompt -i")

